﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 6 tablas
DROP DATABASE IF EXISTS b20190603;
CREATE DATABASE b20190603;

-- Seleccionar la base de datos
USE b20190603;

/*
  creando la tabla clientes
*/

CREATE TABLE accidnts(
  cod int,
  fech date,
  situac varchar(50),
  daño decimal,
PRIMARY KEY(cod) -- creando la clave
  );

CREATE TABLE clientes(
  nif int AUTO_INCREMENT,
  PRIMARY KEY(nif)
);


CREATE TABLE coches(
  nº_matrcl float,
  fech_compr date,
  modelo varchar(100),
  nº_accdnts int,
PRIMARY KEY(nº_matrcl) -- creando la clave
  );

CREATE TABLE tienen(
  nif int,
  cod int,
  fech date,
  daño float,
 PRIMARY KEY(nif,cod), -- creando la clave
 CONSTRAINT fkcltestienen FOREIGN KEY(nif)
 REFERENCES clientes(nif),
 CONSTRAINT fktienenaccidnts FOREIGN KEY(cod)
 REFERENCES accidnts(cod)
);

CREATE TABLE poseen(
  nif int,
  nº_matrcl float,
  fech_compr date,
  modelo varchar(100),
  nº_accdnts int,
  UNIQUE KEY(nº_matrcl),
PRIMARY KEY(nif,nº_matrcl), -- creando la clave
CONSTRAINT fkcltesposeen FOREIGN KEY(nif)
REFERENCES  clientes(nif),
CONSTRAINT fktienencoches FOREIGN KEY(nº_matrcl)
REFERENCES coches(nº_matrcl)



);

                                            
                                            




